// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import axios, { AxiosResponse } from 'axios';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
	const showWaifu = (checkBox:Boolean = false) => {
		const config = vscode.workspace.getConfiguration('daily-waifu');

		const type = config.get("nsfw") ? 'nsfw' : 'sfw';

		axios.get(`https://api.waifu.pics/${type}/waifu`).then((resp: AxiosResponse) => {
			const panel = vscode.window.createWebviewPanel(
				'waifuView',
				'Waifu uwu',
				vscode.ViewColumn.One,
				{
          // Enable scripts in the webview
          enableScripts: true
        }
			);

			panel.webview.onDidReceiveMessage(
        message => {
          switch (message.command) {
            case 'preventToday':
              preventToday();
              return;
						case 'keepThrowing':
							preventToday(false);
							return;
          }
        },
        undefined,
        context.subscriptions
      );

			try {
				if(!checkBox) {
					panel.webview.html = 
						`
						<img src="${resp.data.url}" width="300">
						`;
					return;
				}
				panel.webview.html = 
				`
					<img src="${resp.data.url}" width="300">
					<br>
					<input type="checkbox" id="preventToday" name="preventToday">
  				<label for="preventToday">This is my only waifu today</label>

					<script>
						const vscode = acquireVsCodeApi();
						const checkbox = document.getElementById('preventToday')

						checkbox.addEventListener('change', (event) => {
							if (event.currentTarget.checked) {
								vscode.postMessage({
									command: 'preventToday'
								})
							} else {
								vscode.postMessage({
									command: 'keepThrowing'
								})
							}
						})
					</script
				`;
			} catch (err) {
				console.error(err);
			}
		});
	};

	const preventToday = (mode:Boolean = true) => {
		if(mode) {
			context.globalState.update("daily-waifu.lastShow", new Date().toDateString());
		}
		else {
			context.globalState.update("daily-waifu.lastShow", new Date(0).toDateString());
		}
	};
	
	const lastShow:String = new Date(context.globalState.get("daily-waifu.lastShow") || 0).toDateString();
	if(new Date().toDateString() !== lastShow) {
		showWaifu(true);
	}

	const throwWaifuDisposable = vscode.commands.registerCommand('daily-waifu.randomWaifu', showWaifu);
	const keepShowWaifuTodayDisposable = vscode.commands.registerCommand('daily-waifu.keepThrowing', () => preventToday(false));

	context.subscriptions.push(throwWaifuDisposable, keepShowWaifuTodayDisposable);
}

// this method is called when your extension is deactivated
export function deactivate() {}
